package config

// DefaultTemplatePathInWorkDir 在工作目录下的模板文件的默认路径
const DefaultTemplatePathInWorkDir = "/xconfig/templates"

// DefaultTemplatePathInExecDir 在执行文件目录下的模板文件的默认路径
const DefaultTemplatePathInExecDir = "/resources/templates"

// TemplateExtName 模版文件的后缀名
const TemplateExtName = ".go.tpl"

// DefaultTemplateCoreDirName 在模板文件下，对应于每一种导出配置的根目录的默认路径
const DefaultTemplateCoreDirName = "core"
