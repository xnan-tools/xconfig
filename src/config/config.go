package config

// CoreMainTemplateName 用于配置导出的主模板文件名
const CoreMainTemplateName = "main"

// DefaultConfigName 模板文件的配置
const DefaultConfigName = "config.json"
