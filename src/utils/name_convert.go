package utils

import (
	"regexp"
	"strings"
)

// ToCamelName 将字符串转换成`小驼峰`命名法的字符串(如getName)
func ToCamelName(name string) string {
	re := specialRegexp()

	splited := re.Split(name, -1)

	strs := make([]string, len(splited))
	strs[0] = splited[0]
	for i, str := range splited[1:] {
		strs[i+1] = strings.TrimSpace(strings.Title(str))
	}

	return strings.Join(strs, "")
}

// ToPascalName 将字符串转换成`帕斯卡`命名法的字符串(如GetName)
func ToPascalName(name string) string {
	re := specialRegexp()

	splited := re.Split(name, -1)
	strs := make([]string, len(splited))
	for i, str := range splited {
		strs[i] = strings.TrimSpace(strings.Title(str))
	}

	return strings.Join(strs, "")
}

func specialRegexp() *regexp.Regexp {
	return regexp.MustCompile("[-_.!@#$%^&*<>\\[\\]{}:\"';,./\\s]+")
}
