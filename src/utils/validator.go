package utils

import "regexp"

// ValidateIdentifierFormat 验证字符串是否是合法的标识符
func ValidateIdentifierFormat(name string) bool {
	r := regexp.MustCompile("^[a-zA-Z_][a-zA-Z0-9_]*$")

	return r.MatchString(name)
}
