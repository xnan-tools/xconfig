package cmdparser

type _CommandOptions map[string]string

// CommandArgs 命令参数结构
type CommandArgs struct {
	InputPath  string
	OutputPath string
	Language   string
	Name       string
	Tag        string
	Options    map[string]string
}
