package cmdparser

import (
	"flag"
	"fmt"
	"os"
	"strings"
)

// ParseCommand 解析命令行参数，并且返回一个已经封装好的命令行参数对象
func ParseCommand() (*CommandArgs, error) {
	options := new(_CommandOptions)
	(*options) = make(map[string]string)

	args := new(CommandArgs)

	flag.StringVar(&args.OutputPath, "o", "", "输出文件的路径")
	flag.StringVar(&args.Tag, "t", "", "指定需要导出的字段的标签(对应配置表中的字段标签)")
	flag.StringVar(&args.Name, "n", "", "配置的名称，将会覆盖输入的配置文件中的'#name'定义")
	flag.StringVar(&args.Language, "l", "", "导出语言")
	flag.Var(options, "O", "定义扩展选项，以key-value的形式作为参数，'='分割，并且可定义多次(如: -O xxx=111 -O yyy=222)")

	flag.Parse()

	noflagArgs := flag.Args()
	if len(noflagArgs) > 0 {
		args.InputPath = flag.Args()[0]
	}
	(*args).Options = *options

	err := _verifyCommandArgs(args)

	return args, err
}

func _verifyCommandArgs(args *CommandArgs) error {
	// verify input path
	if len(args.InputPath) == 0 {
		return fmt.Errorf("no input file")
	}
	_, err := os.Open(args.InputPath)
	if os.IsNotExist(err) {
		return fmt.Errorf("input file '%v' is not found", args.InputPath)
	}

	// verify language
	if len(args.Language) == 0 {
		return fmt.Errorf("must be use option '-l' specify export language")
	}

	return nil
}

func (options *_CommandOptions) String() string {
	return fmt.Sprint(*options)
}

func (options *_CommandOptions) Set(value string) error {
	pairNum := 2
	optionPair := strings.SplitN(value, "=", pairNum)
	optionKey := optionPair[0]
	optionValue := ""

	if len(optionPair) >= pairNum {
		optionValue = optionPair[1]
	}

	(*options)[optionKey] = optionValue

	return nil
}
