package validator

import "regexp"

var integerRegexp = regexp.MustCompile("^-?[0-9]+$")
var uintegerRegexp = regexp.MustCompile("^[0-9]+$")
var floatRegexp = regexp.MustCompile("^-?[0-9]+([.][0-9]+)?$")
var boolRegexp = regexp.MustCompile("true|false")

var gFieldValidateRuleMap = map[string]*regexp.Regexp{
	"int":    integerRegexp,
	"uint":   uintegerRegexp,
	"int64":  integerRegexp,
	"uint64": uintegerRegexp,
	"float":  floatRegexp,
	"double": floatRegexp,
	"bool":   boolRegexp,
}
