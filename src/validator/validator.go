package validator

import (
	"fmt"
	"xconfig/src/parser"
)

// Validate 验证配置数据是否合法
func Validate(configData *parser.ConfigureData) error {
	for rowID, row := range configData.DataRows {
		for _, fieldData := range row.Datas {
			fieldType := fieldData.Meta.TypeName
			if !_verifyFieldType(fieldType, fieldData.Value) {
				return fmt.Errorf(
					"Verify field '%v' in row '%v' error: value '%v' is not a '%v'",
					fieldData.Meta.Name,
					configData.DataStartRows+rowID+1,
					fieldData.Value,
					fieldData.Meta.TypeName,
				)
			}
		}
	}

	return nil
}

func _verifyFieldType(typeName string, value string) bool {
	regexp, ok := gFieldValidateRuleMap[typeName]
	if ok {
		return regexp.MatchString(value)
	}

	return true
}
