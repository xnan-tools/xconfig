package app

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"xconfig/src/cmdparser"
	"xconfig/src/config"
	"xconfig/src/parser"
	"xconfig/src/reader"
	"xconfig/src/tplbuilder"
	"xconfig/src/validator"
)

// Run 这个应用程序的入口
func Run() error {
	args, err := cmdparser.ParseCommand()
	if err != nil {
		return err
	}

	configData, err := _parseConfigData(args)
	if err != nil {
		return err
	}

	tbuilderCtx := _genTemplateBuilder(args)
	tpl, err := tplbuilder.BuildTemplate(tbuilderCtx)
	if err != nil {
		return err
	}

	_mergeConfig(args, configData)

	if err = validator.Validate(configData); err != nil {
		return err
	}

	return exportConfigs(tpl, tbuilderCtx, args, configData)
}

func _mergeConfig(args *cmdparser.CommandArgs, configData *parser.ConfigureData) {
	if len(args.Name) > 0 {
		configData.Macros.Name = args.Name
	}

	for k, v := range args.Options {
		configData.Macros.Options[k] = v
	}
}

func _parseConfigData(args *cmdparser.CommandArgs) (*parser.ConfigureData, error) {
	var dataReader reader.ConfigureReader

	inputPathExt := path.Ext(args.InputPath)
	switch inputPathExt {
	case ".xlsx":
		dataReader = new(reader.ExcelReader)

	case ".csv":
		dataReader = new(reader.CsvReader)
	}

	if dataReader == nil {
		return nil, fmt.Errorf("Error: not supported file '" + inputPathExt + "'")
	}

	srcData, err := dataReader.Read(args.InputPath)
	if err != nil {
		return nil, err
	}

	return parser.Parse(args, srcData)
}

func _genTemplateBuilder(args *cmdparser.CommandArgs) *tplbuilder.TemplateBuilderContext {
	wd, _ := os.Getwd()
	execPath, _ := filepath.Abs(os.Args[0])
	execDir := path.Dir(execPath)

	wd += config.DefaultTemplatePathInWorkDir
	execDir += config.DefaultTemplatePathInExecDir

	tbuilderCtx := new(tplbuilder.TemplateBuilderContext)
	tbuilderCtx.CoreDirName = config.DefaultTemplateCoreDirName
	tbuilderCtx.CoreName = args.Language
	tbuilderCtx.SearchPaths = []string{
		wd,
		execDir,
	}

	return tbuilderCtx
}
