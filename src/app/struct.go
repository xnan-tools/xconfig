package app

type _ExportConfigData struct {
	ExtensionName  *string
	TemplateName   *string
	ExportNameType *string
}
