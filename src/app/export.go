package app

import (
	"encoding/json"
	"fmt"
	"os"
	"text/template"
	"xconfig/src/cmdparser"
	"xconfig/src/config"
	"xconfig/src/parser"
	"xconfig/src/tplbuilder"
	"xconfig/src/utils"
)

func exportConfigs(
	tpl *template.Template,
	tbuilderCtx *tplbuilder.TemplateBuilderContext,
	args *cmdparser.CommandArgs,
	configData *parser.ConfigureData,
) error {
	configs, err := _getExportConfigs(tbuilderCtx)
	if err != nil {
		return err
	}

	for _, conf := range configs {
		if err := _exportConfig(tpl, conf, args, configData); err != nil {
			return err
		}
	}

	return nil
}

func _exportConfig(
	tpl *template.Template,
	exportConfig *_ExportConfigData,
	args *cmdparser.CommandArgs,
	configData *parser.ConfigureData,
) error {

	exportPath := _getExportPath(args, exportConfig, configData)

	wf, err := os.OpenFile(exportPath, (os.O_WRONLY | os.O_CREATE | os.O_TRUNC), 0644)
	if err != nil {
		return fmt.Errorf("Error : open file '" + exportPath + "' error")
	}
	tplName := _getTemplateName(exportConfig)

	if err = tpl.ExecuteTemplate(wf, tplName, configData); err != nil {
		return err
	}

	return nil
}

func _getExportConfigs(tbuilderCtx *tplbuilder.TemplateBuilderContext) ([]*_ExportConfigData, error) {
	exportConfigs := make([]*_ExportConfigData, 0)

	confPath := ""
	var content *string = nil

	for _, path := range tbuilderCtx.SearchPaths {
		confPath := path + "/" + tbuilderCtx.CoreDirName + "/" + tbuilderCtx.CoreName + "/" + config.DefaultConfigName
		content = _readExportConfig(confPath)
		if content != nil {
			break
		}
	}

	if content == nil {
		exportConfigs = append(exportConfigs, _createDefaultExportConfig())
	} else {
		if err := json.Unmarshal([]byte(*content), &exportConfigs); err != nil {
			return nil, fmt.Errorf("Error: json file '%s' is invalid", confPath)
		}
	}

	return exportConfigs, nil
}

func _readExportConfig(path string) *string {
	f, openErr := os.Open(path)

	if os.IsNotExist(openErr) {
		return nil
	}

	stat, err := f.Stat()
	if err != nil {
		return nil
	}

	content := make([]byte, stat.Size())
	f.Read(content)

	s := string(content)

	return &s
}

func _getExportPath(
	args *cmdparser.CommandArgs,
	exportConfig *_ExportConfigData,
	configData *parser.ConfigureData,
) string {
	exportDir := _getExportDir(args)
	exportName := _genExportName(configData.Macros.Name, exportConfig)
	extName := ""
	if exportConfig.ExtensionName == nil {
		extName = args.Language
	} else {
		extName = *exportConfig.ExtensionName
	}

	return exportDir + "/" + exportName + "." + extName
}

func _genExportName(name string, exportConfig *_ExportConfigData) string {
	if exportConfig.ExportNameType == nil {
		return name
	}

	switch *exportConfig.ExportNameType {
	case gExportNameTypeCamel:
		return utils.ToCamelName(name)

	case gExportNameTypePascal:
		return utils.ToPascalName(name)
	}

	return name
}

func _getExportDir(args *cmdparser.CommandArgs) string {
	outputPath, _ := os.Getwd()

	if len(args.OutputPath) > 0 {
		outputPath = args.OutputPath
	}

	return outputPath
}

func _createDefaultExportConfig() *_ExportConfigData {
	return new(_ExportConfigData)
}

func _getTemplateName(exportConfig *_ExportConfigData) string {
	if exportConfig.TemplateName == nil {
		return config.CoreMainTemplateName
	}

	return *exportConfig.TemplateName
}
