package reader

import (
	"fmt"

	"github.com/360EntSecGroup-Skylar/excelize"
)

// ExcelReader 针对Microsoft的office excel表格的解析器
type ExcelReader struct {
}

func (reader *ExcelReader) Read(filePath string) ([][]string, error) {
	xlsx, err := excelize.OpenFile(filePath)
	if err != nil {
		return nil, fmt.Errorf("Error : not found file '%v'", filePath)
	}
	sheetName := xlsx.GetSheetName(1)
	srcData := xlsx.GetRows(sheetName)

	return srcData, nil
}
