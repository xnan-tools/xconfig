package reader

// ConfigureReader 解析器接口
type ConfigureReader interface {
	Read(filePath string) ([][]string, error)
}
