package reader

import (
	"encoding/csv"
	"os"
)

// CsvReader CSV表格的解析器
type CsvReader struct {
}

func (reader *CsvReader) Read(filePath string) ([][]string, error) {
	f, err := os.Open(filePath)

	if err != nil {
		return nil, err
	}

	csvReader := csv.NewReader(f)
	csvReader.FieldsPerRecord = -1
	srcData, err := csvReader.ReadAll()

	return srcData, err
}
