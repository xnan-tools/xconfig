package tplbuilder

// CommonPathName 通用模板路径的名称
const CommonPathName = "common"

// CommonPrefix 通用模板名称的前缀
const CommonPrefix = "Common."

// TemplateBuilderContext 用于构建模板构建器对象的上下文结构
type TemplateBuilderContext struct {
	SearchPaths []string
	CoreName    string
	CoreDirName string
}
