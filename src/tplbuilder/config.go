package tplbuilder

import "xconfig/src/config"

const gCommonPathName = "common"
const gCommonPrefix = gCommonPathName + "/"
const gTemplateExtName = config.TemplateExtName
const gTypeValuePrefix = "type-value/"
