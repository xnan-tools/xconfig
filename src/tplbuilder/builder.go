package tplbuilder

import (
	"bytes"
	"encoding/json"
	"os"
	"path"
	"reflect"
	"strings"
	"text/template"
	"xconfig/src/parser"
	"xconfig/src/utils"
)

// BuildTemplate 构建golang标准库中的text/template结构作为导出模板对象
func BuildTemplate(ctx *TemplateBuilderContext) (*template.Template, error) {
	tpl := template.New(ctx.CoreName)

	funcMap := _genTemplateFunctions(tpl)
	tpl.Funcs(funcMap)

	tplPathMap := _searchAllTemplatePathMap(ctx)
	for tplName, tplPath := range tplPathMap {
		subTmpl := tpl.New(tplName)
		content, err := _readTemplateContent(tplPath)
		if err != nil {
			return nil, err
		}

		subTmpl.Parse(content)
	}

	return tpl, nil
}

func _genTemplateFunctions(tpl *template.Template) template.FuncMap {
	funcMap := template.FuncMap{
		"valueWithFieldData": func(data *parser.FieldData) string {
			typeTmplName := gCommonPrefix + gTypeValuePrefix + data.Meta.TypeName
			buf := new(bytes.Buffer)
			tpl.ExecuteTemplate(buf, typeTmplName, data.Value)
			return buf.String()
		},
		"addInt":        func(l int, r int) int { return l + r },
		"subInt":        func(l int, r int) int { return l - r },
		"toCamelName":   utils.ToCamelName,
		"toPascalName":  utils.ToPascalName,
		"stringRepeat":  strings.Repeat,
		"stringReplace": func(str string, oldStr string, newStr string) string { return strings.Replace(str, oldStr, newStr, -1) },
		"parseJson": func(jsonStr string) interface{} {
			jsonBytes := []byte(jsonStr)

			var jsonMap map[string]interface{}
			err := json.Unmarshal(jsonBytes, &jsonMap)
			if err == nil {
				return jsonMap
			}

			var jsonArr []interface{}
			err = json.Unmarshal(jsonBytes, &jsonArr)
			if err == nil {
				return jsonArr
			}

			return nil
		},
		"typeof": func(obj interface{}) string { return reflect.TypeOf(obj).String() },
	}
	return funcMap
}

func _searchAllTemplatePathMap(ctx *TemplateBuilderContext) map[string]string {
	tplPathMap := make(map[string]string)

	_searchCoreTemplatePaths(ctx, &tplPathMap)
	_searchCommonTemplatePaths(ctx, &tplPathMap)

	return tplPathMap
}

func _searchCoreTemplatePaths(ctx *TemplateBuilderContext, tplPathMap *map[string]string) {
	for _, searchPath := range ctx.SearchPaths {
		rootPath := path.Join(searchPath, ctx.CoreDirName, ctx.CoreName)
		_recursionDirForTemplateMap(rootPath, "", tplPathMap)
	}
}

func _searchCommonTemplatePaths(ctx *TemplateBuilderContext, tplPathMap *map[string]string) {
	for _, searchPath := range ctx.SearchPaths {
		_recursionDirForTemplateMap(searchPath, gCommonPathName, tplPathMap)
	}
}

func _recursionDirForTemplateMap(rootDir string, dirPath string, tplPathMap *map[string]string) error {
	listDirPath := path.Join(rootDir, dirPath)
	fd, openErr := os.Open(listDirPath)

	if os.IsNotExist(openErr) {
		return openErr
	}

	names, err := fd.Readdirnames(0)
	if err != nil {
		return err
	}

	for _, name := range names {
		subPath := path.Join(listDirPath, name)

		subFd, openErr := os.Open(subPath)
		if openErr != nil {
			continue
		}

		subStat, err := subFd.Stat()
		if err != nil {
			continue
		}
		if subStat.IsDir() {
			nextDirPath := path.Join(dirPath, name)
			_recursionDirForTemplateMap(rootDir, nextDirPath, tplPathMap)
			continue
		}

		if !strings.HasSuffix(name, gTemplateExtName) {
			continue
		}

		keyName := strings.TrimSuffix(path.Join(dirPath, name), gTemplateExtName)

		_, ok := (*tplPathMap)[keyName]
		if ok {
			continue
		}

		(*tplPathMap)[keyName] = path.Join(listDirPath, name)
	}

	return nil
}

func _readTemplateContent(path string) (string, error) {
	fd, err := os.Open(path)

	if err != nil {
		return "", err
	}

	stat, err := fd.Stat()
	if err != nil {
		return "", err
	}

	b := make([]byte, stat.Size())
	fd.Read(b)

	return string(b), nil
}
