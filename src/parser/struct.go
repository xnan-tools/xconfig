package parser

// FieldMeta 字段元信息
type FieldMeta struct {
	Tags     []string
	TypeName string
	Name     string
	Comment  string
	Col      int
}

// FieldData 字段信息，包含字段元信息和字段值
type FieldData struct {
	Meta  *FieldMeta
	Value string
	Row   int
	Col   int
}

// FieldDataRow 数据列表，用于表达一行记录
type FieldDataRow struct {
	Datas []*FieldData
}

// UniqueIndex 作为唯一索引的字段信息
type UniqueIndex struct {
	Meta  *FieldMeta
	Datas map[string]int
}

// MultipleIndex 作为多重索引的字段信息(多重索引是指某个字段的记录中的值存在多个的类型)
type MultipleIndex struct {
	Meta  *FieldMeta
	Datas map[string][]int
}

// ComplexIndex 作为混合索引的字段信息(混合索引是指某行记录是以多个字段值混合作为唯一索引的类型)
type ComplexIndex struct {
	Name  string
	Metas []*FieldMeta
	Datas map[string]int
}

// MacrosIndexConfig 支持的索引类型
type MacrosIndexConfig struct {
	UniqueIndices   []*UniqueIndex
	MultipleIndices []*MultipleIndex
	ComplexIndices  []*ComplexIndex
}

// MacrosConfig 配置中的宏定义
type MacrosConfig struct {
	Name    string
	Index   *MacrosIndexConfig
	Options map[string]string
}

// ConfigureData 完整的配置信息
type ConfigureData struct {
	Macros        *MacrosConfig
	FieldMetas    []*FieldMeta
	DataRows      []*FieldDataRow
	StartRows     int
	DataStartRows int
}

// 用于存储源表格数据的信息
type _SrcDataContext struct {
	row  int
	data [][]string
}

// 用于存储源表格单行数据的信息
type _SrcRowDataContext struct {
	row  int
	data []string
}

// 用于预解析时的唯一索引宏定义的临时存储结构
type _UniqueIndexMacros struct {
	fieldName string
}

// 用于预解析时的多重索引宏定义的临时存储结构
type _MultipleIndexMacros struct {
	fieldName string
}

// 用于预解析时的混合索引宏定义的存储结构
type _ComplexIndexMacros struct {
	indexName  string
	fieldNames []string
}

// 用于预解析时的链接其他表的宏定义的存储结构
type _LinkMacros struct {
	fieldName     string
	filePath      string
	linkFieldName string
}

// 用于预解析时的所有支持的索引的存储结构
type _IndexMacros struct {
	uniqueIndices   []*_UniqueIndexMacros
	multipleIndices []*_MultipleIndexMacros
	complexIndices  []*_ComplexIndexMacros
}

// 用于预解析时的宏定义配置的存储结构
type _PreMacrosConfig struct {
	name    *string
	index   *_IndexMacros
	links   []*_LinkMacros
	options map[string]string
}

// 用于预解析时的宏定义列表结构
type _MacrosList [][]string
type _MacrosMap map[string]_MacrosList
