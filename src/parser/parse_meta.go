package parser

import (
	"fmt"
	"strconv"
	"strings"
	"xconfig/src/cmdparser"
)

func parseFieldMeta(args *cmdparser.CommandArgs, metaDataCtx *_SrcDataContext) ([]*FieldMeta, error) {
	if len(metaDataCtx.data) < gFieldMetaCount {
		return nil, fmt.Errorf("field meta line must be equal or greater than %v", strconv.Itoa(gFieldMetaCount))
	}

	fieldMetas := make([]*FieldMeta, 0)

	for i, name := range metaDataCtx.data[gFieldMetaNameIndex] {
		if len(name) == 0 {
			break
		}

		tags := metaDataCtx.data[gFieldMetaTagIndex]
		types := metaDataCtx.data[gFieldMetaTypeIndex]
		comments := metaDataCtx.data[gFieldMetaCommentIndex]

		tagArrs := strings.Split(tags[i], ",")
		if !_validTags(args.Tag, tagArrs) {
			continue
		}

		fieldMeta := new(FieldMeta)

		fieldMeta.Tags = tagArrs
		fieldMeta.TypeName = types[i]
		fieldMeta.Name = name
		fieldMeta.Comment = comments[i]
		fieldMeta.Col = i

		fieldMetas = append(fieldMetas, fieldMeta)
	}

	return fieldMetas, nil
}

func _validTags(tag string, tagArrs []string) bool {
	for _, tagStr := range tagArrs {
		if tag == tagStr || tagStr == gTagWildcardCharacter {
			return true
		}
	}

	return false
}
