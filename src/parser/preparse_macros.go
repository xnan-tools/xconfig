package parser

import (
	"strings"
)

// pre-parse macros in configure
func preparseMacrosConfig(data []string) *_PreMacrosConfig {
	macrosConfig := new(_PreMacrosConfig)
	macrosConfig.options = make(map[string]string)

	macrosMap := _splitMacrosConfig(data)
	for k, macrosList := range macrosMap {
		switch k {
		case gSupportedMacrosName:
			macrosConfig.name = _preparseNameMacros(macrosList)
		case gSupportedMacrosIndex:
			macrosConfig.index = _preparseIndexMacros(macrosList)
		case gSupportedMacrosLink:
			macrosConfig.links = _preparseLinkMacros(macrosList)
		case gSupportedMacrosOption:
			macrosConfig.options = _preparseOptionMacros(macrosList)
		}
	}

	return macrosConfig
}

func _preparseNameMacros(macrosList _MacrosList) *string {
	macros := macrosList[0]
	name := new(string)
	*name = macros[0]
	return name
}

func _preparseIndexMacros(macrosList _MacrosList) *_IndexMacros {
	_IndexMacros := new(_IndexMacros)

	for _, macros := range macrosList {
		indexType := macros[0]

		switch indexType {
		case gIndexTypeUnique:
			_IndexMacros.uniqueIndices = append(_IndexMacros.uniqueIndices, _preparseUniqueIndexMacros(macros[1:]))
		case gIndexTypeMultiple:
			_IndexMacros.multipleIndices = append(_IndexMacros.multipleIndices, _preparseMultipleIndexMacros(macros[1:]))
		case gIndexTypeComplex:
			_IndexMacros.complexIndices = append(_IndexMacros.complexIndices, _preparseComplexIndexMacros(macros[1:]))
		}
	}

	return _IndexMacros
}

func _preparseUniqueIndexMacros(config []string) *_UniqueIndexMacros {
	_IndexMacros := new(_UniqueIndexMacros)
	_IndexMacros.fieldName = config[0]
	return _IndexMacros
}

func _preparseMultipleIndexMacros(config []string) *_MultipleIndexMacros {
	_IndexMacros := new(_MultipleIndexMacros)
	_IndexMacros.fieldName = config[0]
	return _IndexMacros
}

func _preparseComplexIndexMacros(config []string) *_ComplexIndexMacros {
	_IndexMacros := new(_ComplexIndexMacros)
	_IndexMacros.indexName = config[0]
	_IndexMacros.fieldNames = config[1:]
	return _IndexMacros
}

func _preparseLinkMacros(macrosList _MacrosList) []*_LinkMacros {
	linkInfos := make([]*_LinkMacros, 0)

	for _, macros := range macrosList {
		linkInfo := new(_LinkMacros)
		if len(macros) < 3 {
			continue
		}
		linkInfo.fieldName = macros[0]
		linkInfo.filePath = macros[1]
		linkInfo.linkFieldName = macros[2]
		linkInfos = append(linkInfos, linkInfo)
	}

	return linkInfos
}

func _preparseOptionMacros(macrosList _MacrosList) map[string]string {
	optionInfos := make(map[string]string, 0)

	for _, macros := range macrosList {
		optionInfos[macros[0]] = macros[1]
	}

	return optionInfos
}

func _splitMacrosConfig(data []string) _MacrosMap {
	macrosMap := make(_MacrosMap)

	for i := range data {
		macros := data[i]
		splited := strings.Fields(macros)

		if len(splited) == 0 {
			continue
		}

		macrosDefined := splited[0]
		if !(len(macrosDefined) > 0 && macrosDefined[0] == gConfigMacrosBeginChar) {
			continue
		}

		macrosName := macrosDefined[1:]
		supported, ok := gSupportedMacros[macrosName]
		if !(ok && supported) {
			continue
		}

		_, ok = macrosMap[macrosName]
		if !ok {
			macrosMap[macrosName] = make([][]string, 0)
		}

		macrosMap[macrosName] = append(macrosMap[macrosName], splited[1:])
	}

	return macrosMap
}
