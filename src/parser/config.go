package parser

const gConfigStartTag = "#"
const gMacrosIndex = 0

// macros
const gConfigMacrosBeginChar = '#'

const gSupportedMacrosName = "name"
const gSupportedMacrosIndex = "index"
const gSupportedMacrosLink = "link"
const gSupportedMacrosOption = "option"

const gIndexTypeUnique = "unique"
const gIndexTypeMultiple = "multiple"
const gIndexTypeComplex = "complex"

const gTagWildcardCharacter = "*"

var gSupportedMacros = map[string]bool{
	gSupportedMacrosName:   true,
	gSupportedMacrosIndex:  true,
	gSupportedMacrosLink:   true,
	gSupportedMacrosOption: true,
}

// field meta define
const gFieldMetaStartIndex = 1
const gFieldMetaTagIndex = 0
const gFieldMetaTypeIndex = 1
const gFieldMetaNameIndex = 2
const gFieldMetaCommentIndex = 3
const gFieldMetaCount = 4
