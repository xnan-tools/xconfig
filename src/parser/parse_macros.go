package parser

import (
	"fmt"
	"path"
	"strings"
	"xconfig/src/utils"
)

type _FieldMetaInfo struct {
	index int
	meta  *FieldMeta
}

func parseMacros(
	filePath string,
	preMacrosConfig *_PreMacrosConfig,
	fieldMetas []*FieldMeta,
	fieldDataRows []*FieldDataRow,
) (*MacrosConfig, error) {
	macrosConfig := new(MacrosConfig)

	var err error
	macrosConfig.Name, err = _resolveName(filePath, preMacrosConfig)
	if err != nil {
		return nil, err
	}

	macrosConfig.Index, err = _parseIndex(preMacrosConfig.index, fieldMetas, fieldDataRows)
	if err != nil {
		return nil, err
	}

	macrosConfig.Options = preMacrosConfig.options

	return macrosConfig, nil
}

func _parseIndex(
	preIndexMacros *_IndexMacros,
	fieldMetas []*FieldMeta,
	fieldDataRows []*FieldDataRow,
) (*MacrosIndexConfig, error) {
	indexConfig := new(MacrosIndexConfig)

	fieldMetaMap := map[string]*_FieldMetaInfo{}
	for i, meta := range fieldMetas {
		metaInfo := new(_FieldMetaInfo)
		metaInfo.index = i
		metaInfo.meta = meta
		fieldMetaMap[meta.Name] = metaInfo
	}

	if preIndexMacros != nil {
		var err error
		indexConfig.UniqueIndices, err = _parseUniqueIndices(preIndexMacros.uniqueIndices, fieldMetaMap, fieldDataRows)
		if err != nil {
			return nil, err
		}

		indexConfig.MultipleIndices, err = _parseMultipleIndices(preIndexMacros.multipleIndices, fieldMetaMap, fieldDataRows)
		if err != nil {
			return nil, err
		}

		indexConfig.ComplexIndices, err = _parseComplexIndices(preIndexMacros.complexIndices, fieldMetaMap, fieldDataRows)
		if err != nil {
			return nil, err
		}
	}

	return indexConfig, nil
}

func _parseUniqueIndices(
	macrosIndices []*_UniqueIndexMacros,
	fieldMetaMap map[string]*_FieldMetaInfo,
	fieldDataRows []*FieldDataRow,
) ([]*UniqueIndex, error) {
	indices := make([]*UniqueIndex, len(macrosIndices))

	for i, index := range macrosIndices {
		metaInfo := fieldMetaMap[index.fieldName]
		if metaInfo == nil {
			return nil, fmt.Errorf("field '%v' is not found", index.fieldName)
		}
		indices[i] = _parseUniqueIndex(index, metaInfo, fieldDataRows)
	}

	return indices, nil
}

func _parseMultipleIndices(
	macrosIndices []*_MultipleIndexMacros,
	fieldMetaMap map[string]*_FieldMetaInfo,
	fieldDataRows []*FieldDataRow,
) ([]*MultipleIndex, error) {
	indices := make([]*MultipleIndex, len(macrosIndices))

	for i, index := range macrosIndices {
		metaInfo := fieldMetaMap[index.fieldName]
		if metaInfo == nil {
			return nil, fmt.Errorf("field '%v' is not found", index.fieldName)
		}
		indices[i] = _parseMultipleIndex(index, metaInfo, fieldDataRows)
	}

	return indices, nil
}

func _parseComplexIndices(
	macrosIndices []*_ComplexIndexMacros,
	fieldMetaMap map[string]*_FieldMetaInfo,
	fieldDataRows []*FieldDataRow,
) ([]*ComplexIndex, error) {
	indices := make([]*ComplexIndex, len(macrosIndices))

	for i, index := range macrosIndices {
		var err error
		indices[i], err = _parseComplexIndex(index, fieldMetaMap, fieldDataRows)
		if err != nil {
			return nil, err
		}
	}

	return indices, nil
}

func _parseUniqueIndex(
	macrosIndex *_UniqueIndexMacros,
	fieldMetaInfo *_FieldMetaInfo,
	fieldDataRow []*FieldDataRow,
) *UniqueIndex {
	index := new(UniqueIndex)
	index.Datas = make(map[string]int)

	index.Meta = fieldMetaInfo.meta
	for i, row := range fieldDataRow {
		index.Datas[row.Datas[fieldMetaInfo.index].Value] = i
	}

	return index
}

func _parseMultipleIndex(
	macrosIndex *_MultipleIndexMacros,
	fieldMetaInfo *_FieldMetaInfo,
	fieldDataRow []*FieldDataRow,
) *MultipleIndex {
	index := new(MultipleIndex)
	index.Datas = make(map[string][]int)

	index.Meta = fieldMetaInfo.meta
	for i, row := range fieldDataRow {
		datas := index.Datas[row.Datas[fieldMetaInfo.index].Value]
		datas = append(datas, i)
		index.Datas[row.Datas[fieldMetaInfo.index].Value] = datas
	}

	return index
}

func _parseComplexIndex(
	macrosIndex *_ComplexIndexMacros,
	fieldMetaMap map[string]*_FieldMetaInfo,
	fieldDataRow []*FieldDataRow,
) (*ComplexIndex, error) {
	fieldCount := len(macrosIndex.fieldNames)

	index := new(ComplexIndex)
	index.Name = macrosIndex.indexName
	index.Metas = make([]*FieldMeta, fieldCount)
	index.Datas = make(map[string]int)

	for i, name := range macrosIndex.fieldNames {
		metaInfo := fieldMetaMap[name]
		if metaInfo == nil {
			return nil, fmt.Errorf("field '%v' is not found", name)
		}
		index.Metas[i] = metaInfo.meta
	}

	for i, row := range fieldDataRow {
		complexNameArr := make([]string, fieldCount)
		for j, name := range macrosIndex.fieldNames {
			complexNameArr[j] = row.Datas[fieldMetaMap[name].index].Value
		}
		complexName := strings.Join(complexNameArr, ".")
		index.Datas[complexName] = i
	}

	return index, nil
}

func _resolveName(filePath string, preMacrosConfig *_PreMacrosConfig) (string, error) {
	name := ""
	var err error = nil

	if preMacrosConfig.name != nil && len(*preMacrosConfig.name) > 0 {
		name = *preMacrosConfig.name
	} else {
		name = _resolveFilepath(filePath)
	}

	if !utils.ValidateIdentifierFormat(name) {
		err = fmt.Errorf("configure name '%v' is invalid identifier", name)
	}

	return name, err
}

func _resolveFilepath(filePath string) string {
	baseName := path.Base(filePath)

	extIndex := strings.LastIndex(baseName, ".")
	if extIndex == -1 {
		return baseName
	}

	return baseName[0:extIndex]
}
