package parser

func parseFieldData(srcDataCtx *_SrcDataContext, fieldMetas []*FieldMeta) []*FieldDataRow {
	dataRows := make([]*FieldDataRow, len(srcDataCtx.data))

	metaNum := len(fieldMetas)

	for i, row := range srcDataCtx.data {
		fieldDataRow := new(FieldDataRow)
		fieldDataRow.Datas = make([]*FieldData, metaNum)

		j := 0
		for _, meta := range fieldMetas {
			fieldData := new(FieldData)

			fieldData.Meta = meta
			fieldData.Value = row[meta.Col]

			fieldDataRow.Datas[j] = fieldData
			j++
		}

		dataRows[i] = fieldDataRow
	}

	return dataRows
}
