package parser

import (
	"fmt"
	"xconfig/src/cmdparser"
)

type _Parser struct {
	args *cmdparser.CommandArgs
	ctx  *_SrcDataContext
}

// Parse 解析配置
func Parse(args *cmdparser.CommandArgs, srcData [][]string) (*ConfigureData, error) {
	validStartRows, err := getValidDataRows(srcData)
	if err != nil {
		return nil, err
	}

	validData := extractValidData(srcData, validStartRows)
	srcDataCtx := createSrcDataContext(validStartRows, validData)

	parser := createParser(args, srcDataCtx)
	configData, err := parser.parse()
	if err != nil {
		return nil, err
	}

	configData.StartRows = validStartRows
	configData.DataStartRows = validStartRows + gFieldMetaStartIndex + gFieldMetaCount

	return configData, nil
}

func createParser(args *cmdparser.CommandArgs, srcDataCtx *_SrcDataContext) *_Parser {
	parser := new(_Parser)

	parser.args = args
	parser.ctx = srcDataCtx

	return parser
}

func (parser *_Parser) parse() (*ConfigureData, error) {
	configData := new(ConfigureData)

	metaEndIndex := gFieldMetaStartIndex + gFieldMetaCount
	dataStartIndex := metaEndIndex

	preMacrosConfig := preparseMacrosConfig(parser.ctx.data[gMacrosIndex][1:])

	var err error
	metaData := parser.ctx.data[gFieldMetaStartIndex:metaEndIndex]
	metaDataCtx := createSrcDataContext(gFieldMetaStartIndex, metaData)
	fieldMetas, err := parseFieldMeta(parser.args, metaDataCtx)
	if err != nil {
		return nil, err
	}

	srcData := parser.ctx.data[dataStartIndex:]
	srcDataCtx := createSrcDataContext(dataStartIndex, srcData)
	dataRows := parseFieldData(srcDataCtx, fieldMetas)
	macros, err := parseMacros(parser.args.InputPath, preMacrosConfig, fieldMetas, dataRows)
	if err != nil {
		return nil, err
	}

	configData.FieldMetas = fieldMetas
	configData.DataRows = dataRows
	configData.Macros = macros

	return configData, err
}

func getValidDataRows(srcData [][]string) (int, error) {
	for i, row := range srcData {
		if len(row) > 0 && row[0] == gConfigStartTag {
			return i, nil
		}
	}

	return 0, fmt.Errorf("invalid source: not a character '#' in row for start")
}

func extractValidData(srcData [][]string, startRows int) [][]string {
	return srcData[startRows:]
}

func createSrcDataContext(row int, data [][]string) *_SrcDataContext {
	ctx := new(_SrcDataContext)

	ctx.row = row
	ctx.data = data

	return ctx
}

func createSrcRowDataContext(row int, data []string) *_SrcRowDataContext {
	ctx := new(_SrcRowDataContext)

	ctx.row = row
	ctx.data = data

	return ctx
}
