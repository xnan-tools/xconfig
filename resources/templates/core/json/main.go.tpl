{{- $rowMaxIndex := (len .DataRows) -}}
{
    "data": [
        {{- range $rowIndex, $row := .DataRows}}
        {{- $maxIndex := (len $row.Datas) }}
        {
            {{- range $index, $fieldData := $row.Datas}}
            "{{toCamelName $fieldData.Meta.Name}}": {{ valueWithFieldData $fieldData -}}
            {{- if (lt (addInt $index 1) $maxIndex) }},{{end -}}
            {{end}}
        }{{- if (lt (addInt $rowIndex 1) $rowMaxIndex) }},{{end -}}
        {{end}}
    ],
    {{ $commonMaxIndex := 0 }}
    "index": {
        "unique": {
            {{- $commonMaxIndex = (len .Macros.Index.UniqueIndices)}}
            {{- range $index, $indexData := .Macros.Index.UniqueIndices}}
            "{{toCamelName $indexData.Meta.Name}}": {
                {{- $maxDataIndex := (len $indexData.Datas) }}
                {{- $valueIndex := 0 }}
                {{- range $name, $indexValue := $indexData.Datas -}}
                "{{$name}}": {{$indexValue}}
                {{- $valueIndex = (addInt $valueIndex 1) -}}
                {{- if (lt $valueIndex $maxDataIndex) }},{{end}}
                {{- end -}}
            }{{if (lt (addInt $index 1) $commonMaxIndex) -}},{{- end}}
            {{- end}}
        },

        "multiple": {
            {{- $commonMaxIndex = (len .Macros.Index.MultipleIndices)}}
            {{- range $index, $indexData := .Macros.Index.MultipleIndices}}
            "{{toCamelName $indexData.Meta.Name}}": {
                {{- $maxDataIndex := (len $indexData.Datas) }}
                {{- $valueIndex := 0 }}
                {{- range $name, $indexValue := $indexData.Datas -}}
                "{{$name}}": [
                    {{- $idsMaxIndex := (len $indexValue) -}}
                    {{- range $idIndex, $id := $indexValue -}}
                    {{$id}}
                    {{- if (lt (addInt $idIndex 1) $idsMaxIndex) }},{{end -}}
                    {{end -}}
                ]
                {{- $valueIndex = (addInt $valueIndex 1) -}}
                {{- if (lt $valueIndex $maxDataIndex) }},{{end}}
                {{- end -}}
            }{{if (lt (addInt $index 1) $commonMaxIndex) -}},{{end}}
            {{- end}}
        },

        "complex": {
            {{- $commonMaxIndex = (len .Macros.Index.ComplexIndices) -}}
            {{- range $index, $indexData := .Macros.Index.ComplexIndices}}
            "{{toCamelName $indexData.Name}}": {
                {{- $maxDataIndex := (len $indexData.Datas) }}
                {{- $valueIndex := 0 }}
                {{- range $name, $indexValue := $indexData.Datas -}}
                "{{$name}}": {{$indexValue}}
                {{- $valueIndex = (addInt $valueIndex 1) -}}
                {{- if (lt $valueIndex $maxDataIndex) }},{{end}}
                {{- end -}}
            }{{if (lt (addInt $index 1) $commonMaxIndex) -}},{{- end}}
            {{- end}}
        }
    }
}