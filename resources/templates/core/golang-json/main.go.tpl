// {{template "common/generated-tips"}}

{{- $package := .Macros.Options.golang_package }}
{{- $pascalModuleName := toPascalName .Macros.Name}}
{{- $camelModuleName := toCamelName .Macros.Name}}
{{- $moduleDataName := printf "%sData" $pascalModuleName}}
{{- $interfaceName := printf "_%sInterface" $pascalModuleName}}
{{- $moduleThisParam := printf "%s *_%s" $camelModuleName $pascalModuleName}}
{{- $loaderCtxName := printf "%s%s" $pascalModuleName "JSONLoaderContext"}}
{{- $moduleStructName := printf "_%sStruct" $pascalModuleName}}
{{- $indexStructName := printf "_%sIndexStruct" $pascalModuleName}}
{{- $indexUniqueStructName := printf "_%sIndexUniqueStruct" $pascalModuleName}}
{{- $indexMultipleStructName := printf "_%sIndexMultipleStruct" $pascalModuleName}}
{{- $indexComplexStructName := printf "_%sIndexComplexStruct" $pascalModuleName}}

package {{ if $package }} {{- $package -}} {{else -}} table {{- end}}

import (
	"encoding/json"
)

// {{$moduleDataName}} data type
type {{$moduleDataName}} struct {
{{- range .FieldMetas}}
	// {{.Comment}}
	{{template "field-filter" (toPascalName .Name)}} {{template "type" .TypeName}} `json:"{{toCamelName .Name}}"`
{{end -}}
}

// {{$pascalModuleName}} interface of {{$pascalModuleName}}
var {{$pascalModuleName}} {{$interfaceName}} = &_{{$pascalModuleName}}{
	datas:    nil,
	isParsed: false,
}

// {{$loaderCtxName}} context of loader
type {{$loaderCtxName}} interface {
	GetJSONData() string
	Parsed() error
	Error(err error)
}

// Get get {{$moduleDataName}} object
func ({{$moduleThisParam}}) Get(index int) *{{$moduleDataName}} {
	return {{$camelModuleName}}.datas.Data[index]
}

{{- range .Macros.Index.UniqueIndices -}}
{{- $pName := toPascalName .Meta.Name -}}
{{- $origCName := toCamelName .Meta.Name}}
{{$cName := printf "_%s" $origCName}}
// GetBy{{$pName}} get {{$moduleDataName}} object by "{{$origCName}}" with unique
func ({{$moduleThisParam}}) GetBy{{$pName}}({{$cName}} {{template "type" .Meta.TypeName}}) (*{{$moduleDataName}}, bool) {
	if idx, ok := {{$camelModuleName}}.datas.Index.Unique.{{$pName}}[{{$cName}}]; ok {
		return {{$camelModuleName}}.datas.Data[idx], true
	}

	return nil, false
}
{{- end}}

{{- range .Macros.Index.MultipleIndices -}}
{{- $pName := toPascalName .Meta.Name -}}
{{- $origCName := toCamelName .Meta.Name}}
{{$cName := printf "_%s" $origCName}}
// GetBy{{$pName}} get {{$moduleDataName}} object by "{{$origCName}}" with multiple
func ({{$moduleThisParam}}) GetBy{{$pName}}({{$cName}} {{template "type" .Meta.TypeName}}) ([]*{{$moduleDataName}}, bool) {
	if indices, ok := {{$camelModuleName}}.datas.Index.Multiple.{{$pName}}[{{$cName}}]; ok {
		r := make([]*{{$moduleDataName}}, len(indices))
		for i, idx := range indices {
			r[i] = {{$camelModuleName}}.datas.Data[idx]
		}
		return r, true
	}

	return nil, false
}
{{- end}}

{{- range .Macros.Index.ComplexIndices}}
{{$pName := toPascalName .Name}}
// GetBy{{$pName}} get {{$moduleDataName}} object by{{range .Metas}} "{{toCamelName .Name}}"{{end}} with complex
func ({{$moduleThisParam}}) GetBy{{$pName}}(
{{- range .Metas}}
{{- $cName := printf "_%s" (toCamelName .Name)}}
	{{$cName}} {{template "type" .TypeName}},
{{- end}}
) (*{{$moduleDataName}}, bool) {
	l := []string {
	{{- range .Metas}}
	{{- $cName := printf "_%s" (toCamelName .Name)}}
		string({{$cName}}),
	{{- end}}
	}

	complexKey := strings.Join(l, ".")
	if idx, ok := {{$camelModuleName}}.datas.Index.Complex.{{$pName}}[complexKey]; ok {
		return {{$camelModuleName}}.datas.Data[idx], true
	}

	return nil, false
	
}
{{- end}}

// Parse parse configure from {{$pascalModuleName}}
func ({{$moduleThisParam}}) Parse(ctx {{$loaderCtxName}}) {
	go {{$camelModuleName}}._parse(ctx)
}

// Parse{{$pascalModuleName}} is parsed from {{$pascalModuleName}}
func ({{$moduleThisParam}}) IsParsed() bool {
	return {{$camelModuleName}}.isParsed
}

type {{$interfaceName}} interface {
	Get(index int) *{{$moduleDataName}}

	// unique index functions
{{- range .Macros.Index.UniqueIndices -}}
{{- $pName := toPascalName .Meta.Name -}}
{{- $cName := printf "_%s" (toCamelName .Meta.Name)}}
	GetBy{{$pName}}({{$cName}} {{template "type" .Meta.TypeName}}) (*{{$moduleDataName}}, bool)
{{- end}}

	// multiple index functions
{{- range .Macros.Index.MultipleIndices -}}
{{- $pName := toPascalName .Meta.Name -}}
{{- $cName := printf "_%s" (toCamelName .Meta.Name)}}
	GetBy{{$pName}}({{$cName}} {{template "type" .Meta.TypeName}}) ([]*{{$moduleDataName}}, bool)
{{- end}}

	// complex index functions
{{- range .Macros.Index.ComplexIndices -}}
{{$pName := toPascalName .Name}}
	GetBy{{$pName}}(
	{{- range .Metas}}
	{{- $cName := printf "_%s" (toCamelName .Name)}}
		{{$cName}} {{template "type" .TypeName}},
	{{- end}}
	) (*{{$moduleDataName}}, bool)
{{- end}}

	Parse(ctx {{$loaderCtxName}})
	IsParsed() bool
}

// {{$pascalModuleName}} struct of {{$pascalModuleName}}
type _{{$pascalModuleName}} struct {
	datas    *{{$moduleStructName}}
	isParsed bool
}

type {{$indexUniqueStructName}} struct {
{{- $uniqueNameMaxLen := 0 -}}
{{- range .Macros.Index.UniqueIndices -}}
	{{- if gt (len .Meta.Name) $uniqueNameMaxLen -}}
		{{- $uniqueNameMaxLen = (len .Meta.Name) -}}
	{{- end -}}
{{- end -}}

{{- range .Macros.Index.UniqueIndices}}
	{{- $spaceCount := subInt $uniqueNameMaxLen (len .Meta.Name)}}
	{{toPascalName .Meta.Name}}{{stringRepeat " " $spaceCount}} map[{{template "type" .Meta.TypeName}}]int `json:"{{toCamelName .Meta.Name}}"`
{{- end -}}
{{- "\n" -}}
}

type {{$indexMultipleStructName}} struct {
{{- $multipleNameMaxLen := 0 -}}
{{- range .Macros.Index.MultipleIndices -}}
	{{- if gt (len .Meta.Name) $multipleNameMaxLen -}}
		{{- $multipleNameMaxLen = (len .Meta.Name) -}}
	{{- end -}}
{{- end -}}

{{- range .Macros.Index.MultipleIndices}}
	{{$spaceCount := subInt $multipleNameMaxLen (len .Meta.Name) -}}
	{{toPascalName .Meta.Name}}{{stringRepeat " " $spaceCount}} map[{{template "type" .Meta.TypeName}}][]int `json:"{{toCamelName .Meta.Name}}"`
{{- end -}}
{{- "\n" -}}
}

type {{$indexComplexStructName}} struct {
{{- $complexNameMaxLen := 0 -}}
{{- range .Macros.Index.ComplexIndices -}}
	{{- if gt (len .Name) $complexNameMaxLen -}}
		{{- $complexNameMaxLen = (len .Name) -}}
	{{- end -}}
{{- end -}}

{{- range .Macros.Index.ComplexIndices}}
	{{$spaceCount := subInt $complexNameMaxLen (len .Name) -}}
	{{toPascalName .Name}}{{stringRepeat " " $spaceCount}} map[string]int `json:"{{toCamelName .Name}}"`
{{- end -}}
{{- "\n" -}}
}

type {{$indexStructName}} struct {
{{- if (ne (len .Macros.Index.UniqueIndices) 0)}}
	Unique   *{{$indexUniqueStructName}} `json:"unique"`
{{- end -}}

{{- if (ne (len .Macros.Index.MultipleIndices) 0)}}
	Multiple *{{$indexMultipleStructName}} `json:"multiple"`
{{- end -}}

{{- if (ne (len .Macros.Index.ComplexIndices) 0)}}
	Complex  *{{$indexComplexStructName}} `json:"complex"`
{{- end}}
}

type {{$moduleStructName}} struct {
	{{$moduleNameLen := (addInt (len $moduleDataName) 2) -}}
	{{- $indexNameLen := (len $indexStructName) -}}
	{{- $jsonObjectMaxLen := 0 -}}
	
	{{- if gt $moduleNameLen $indexNameLen -}}
		{{- $jsonObjectMaxLen = $moduleNameLen -}}
	{{- else -}}
		{{- $jsonObjectMaxLen = $indexNameLen -}}
	{{- end -}}

	Data  []*{{$moduleDataName}}{{stringRepeat " " (subInt $jsonObjectMaxLen $moduleNameLen)}} `json:"data"`
	Index *{{$indexStructName}}{{stringRepeat " " (subInt $jsonObjectMaxLen $indexNameLen)}} `json:"index"`
}

func ({{$moduleThisParam}}) _parse(ctx {{$loaderCtxName}}) {
	d := new({{$moduleStructName}})

	dataBytes := []byte(ctx.GetJSONData())
	err := json.Unmarshal(dataBytes, d)
	if err != nil {
		ctx.Error(err)
	} else {
		{{$camelModuleName}}.isParsed = true
		{{$camelModuleName}}.datas = d
		ctx.Parsed()
	}
}
