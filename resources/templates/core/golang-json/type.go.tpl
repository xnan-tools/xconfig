{{- if eq . "bool" -}}
bool
{{- else if eq . "int" -}}
int
{{- else if eq . "uint" -}}
uint
{{- else if eq . "int64" -}}
int64
{{- else if eq . "uint64" -}}
uint64
{{- else if eq . "float" -}}
float32
{{- else if eq . "double" -}}
float64
{{- else if eq . "string" -}}
string
{{- else -}}
interface{}
{{- end -}}
