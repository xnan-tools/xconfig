{{/* Root Param : FieldMeta */}}
{{- if eq .TypeName "bool" -}}
bool
{{- else if eq .TypeName "int" -}}
int
{{- else if eq .TypeName "uint" -}}
uint
{{- else if eq .TypeName "int64" -}}
long
{{- else if eq .TypeName "uint64" -}}
ulong
{{- else if eq .TypeName "float" -}}
float
{{- else if eq .TypeName "double" -}}
float
{{- else if eq .TypeName "string" -}}
string
{{- else if eq .TypeName "json" -}}
JObject
{{- else if eq .TypeName "array" -}}
JArray
{{- else -}}
string
{{- end -}}
