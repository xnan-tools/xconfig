// {{template "common/generated-tips"}}

{{- $ns := .Macros.Options.csharp_namespace}}
{{- $pascalModuleName := toPascalName .Macros.Name}}
{{- $camelModuleName := toCamelName .Macros.Name}}
{{- $moduleDataName := printf "%sData" $pascalModuleName}}
{{- $indexDataName := printf "%sIndexData" $pascalModuleName}}
{{- $indexUniqueDataName := printf "%sIndexUniqueData" $pascalModuleName}}
{{- $indexMultipleDataName := printf "%sIndexMultipleData" $pascalModuleName}}
{{- $indexComplexDataName := printf "%sIndexComplexData" $pascalModuleName}}

using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace {{ if $ns }} {{- $ns -}} {{else -}} Tables {{- end}} {
    public class {{$pascalModuleName}} {
        protected {{$pascalModuleName}} () {}

        public static {{$pascalModuleName}} Instance {
            get {
                return {{$pascalModuleName}}.instance;
            }
        }

        public {{$moduleDataName}} Get(int index) {
            return this.All()[index];
        }

        public List<{{$moduleDataName}}> All() {
            return this.data.data;
        }

        {{- range .Macros.Index.UniqueIndices}}
        {{- $pName := toPascalName .Meta.Name -}}
        {{- $origCName := toCamelName .Meta.Name -}}
        {{- $cName := printf "_%s" $origCName}}
        {{""}}
        // get {{$moduleDataName}} object by "{{$origCName}}" with unique
        public {{$moduleDataName}} GetBy{{$pName}}({{template "type" .Meta}} {{$cName}}) {
            return this.Get(this.data.index.unique.{{$cName}}[{{$cName}}]);
        }

        // contains key by "{{$origCName}}" with unique
        public bool ContainsKeyBy{{$pName}}({{template "type" .Meta}} {{$cName}}) {
            return this.data.index.unique.{{$cName}}.ContainsKey({{$cName}});
        }
        {{- end}}

        {{- range .Macros.Index.MultipleIndices}}
        {{- $pName := toPascalName .Meta.Name -}}
        {{- $origCName := toCamelName .Meta.Name -}}
        {{- $cName := printf "_%s" $origCName}}
        {{""}}
        // get {{$moduleDataName}} object by "{{$origCName}}" with multiple
        public List<{{$moduleDataName}}> GetBy{{$pName}}({{template "type" .Meta}} {{$cName}}) {
            List<int> indices = this.data.index.multiple.{{$cName}}[{{$cName}}];

            List<{{$moduleDataName}}> l = new List<{{$moduleDataName}}>();
            foreach (int index in indices) {
                l.Add(this.Get(index));
            }
            
            return l;
        }

        // contains key by "{{$origCName}}" with multiple
        public bool ContainsKeyBy{{$pName}}({{template "type" .Meta}} {{$cName}}) {
            return this.data.index.multiple.{{$cName}}.ContainsKey({{$cName}});
        }
        {{- end}}

        {{- range .Macros.Index.ComplexIndices}}
        {{- $pName := toPascalName .Name -}}
        {{- $origCName := toCamelName .Name -}}
        {{- $cName := printf "_%s" $origCName}}
        {{- $metasCount := len .Metas}}
        {{- $maxMetasIndex := subInt $metasCount 1}}
        {{""}}
        // get {{$moduleDataName}} object by{{range .Metas}} "{{toCamelName .Name}}"{{end}} with complex
        public {{$moduleDataName}} GetBy{{$pName}}(
        {{- range $index, $meta := .Metas}}
        {{- $metaCName := printf "_%s" (toCamelName $meta.Name)}}
            {{template "type" $meta}} {{$metaCName}}{{if lt $index $maxMetasIndex}},{{end}}
        {{- end}}
        ) {
            string[] keys = {
            {{- range .Metas}}
            {{- $metaCName := printf "_%s" (toCamelName .Name)}}
                {{$metaCName}}.ToString(),
            {{- end}}
            };

            string key = string.Join(".", keys);
            
            return this.Get(this.data.index.complex.{{$cName}}[key]);
        }

        // contains key by{{range .Metas}} "{{toCamelName .Name}}"{{end}} with complex
        public bool ContainsKeyBy{{$pName}}(
        {{- range $index, $meta := .Metas}}
        {{- $metaCName := printf "_%s" (toCamelName $meta.Name)}}
            {{template "type" $meta}} {{$metaCName}}{{if lt $index $maxMetasIndex}},{{end}}
        {{- end}}
        ) {
            string[] keys = {
            {{- range .Metas}}
            {{- $metaCName := printf "_%s" (toCamelName .Name)}}
                {{$metaCName}}.ToString(),
            {{- end}}
            };

            string key = string.Join(".", keys);
            return this.data.index.complex.{{$cName}}.ContainsKey(key);
        }
        {{- end}}

        public Task Parse(string jsonStr) {
            return Task.Run(() => {
                this.data = JsonConvert.DeserializeObject<{{$pascalModuleName}}Root>(jsonStr);
            });
        }
        
        protected {{$pascalModuleName}}Root data;
        protected static {{$pascalModuleName}} instance = new {{$pascalModuleName}}();
    }

    public class {{$pascalModuleName}}Root {
        [JsonProperty("data")]
        public List<{{$moduleDataName}}> data { get; set; }

        [JsonProperty("index")]
        public {{$indexDataName}} index { get; set; }
    }

    public class {{$moduleDataName}} {
    {{- range .FieldMetas}}
        // {{.Comment}}
        public {{template "type" .}} {{toPascalName .Name}} { get { return _{{toCamelName .Name}}; } }
    {{- end}}
    {{range .FieldMetas}}
        [JsonProperty("{{toCamelName .Name}}")]
        protected {{template "type" .}} _{{toCamelName .Name}} { get; set; }
    {{end}}
    }

    public class {{$indexDataName}} {
        [JsonProperty("unique")]
        public {{$indexUniqueDataName}} unique { get; set; }

        [JsonProperty("multiple")]
        public {{$indexMultipleDataName}} multiple { get; set; }

        [JsonProperty("complex")]
        public {{$indexComplexDataName}} complex { get; set; }
    }

    public class {{$indexUniqueDataName}} {
    {{- range .Macros.Index.UniqueIndices -}}
    {{- $pName := toPascalName .Meta.Name -}}
    {{- $origCName := toCamelName .Meta.Name -}}
    {{$cName := printf "_%s" $origCName}}
        [JsonProperty("{{$origCName}}")]
        public Dictionary<{{template "type" .Meta}}, int> {{$cName}} { get; set; }
    {{end -}}
    }

    public class {{$indexMultipleDataName}} {
    {{- range .Macros.Index.MultipleIndices -}}
    {{- $pName := toPascalName .Meta.Name -}}
    {{- $origCName := toCamelName .Meta.Name -}}
    {{$cName := printf "_%s" $origCName}}
        [JsonProperty("{{$origCName}}")]
        public Dictionary<{{template "type" .Meta}}, List<int>> {{$cName}} { get; set; }
    {{end -}}
    }

    public class {{$indexComplexDataName}} {
    {{- range .Macros.Index.ComplexIndices -}}
    {{- $pName := toPascalName .Name -}}
    {{- $origCName := toCamelName .Name -}}
    {{$cName := printf "_%s" $origCName}}
        [JsonProperty("{{$origCName}}")]
        public Dictionary<string, int> {{$cName}} { get; set; }
    {{end -}}
    }
}
