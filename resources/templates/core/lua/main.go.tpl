-- {{template "common/generated-tips"}}

{{- $module := .Macros.Options.lua_module }}
{{- $moduleName := toPascalName .Macros.Name }}

{{- if $module }}
{{- $moduleName = printf "%s.%s" $module $moduleName}}
{{- end}}

{{- $this := .}}

--[[ {{$moduleName}} structure
{{range .FieldMetas -}}
- {{.Name}} : <{{.TypeName}}> {{.Comment}}
{{end -}}
]]
{{ $moduleName }} = {}

{{ $moduleName }}.get = function(index)
    return {{ $moduleName }}._data[index]
end

{{ $moduleName }}.all = function()
    return {{ $moduleName }}._data
end

{{range .Macros.Index.UniqueIndices -}}
{{ $moduleName }}.getBy{{ toPascalName .Meta.Name }} = function({{toCamelName .Meta.Name}})
    local key = {{$moduleName}}._index.unique.{{ toCamelName .Meta.Name }}[tostring({{toCamelName .Meta.Name}})]
    return {{ $moduleName }}.get(key)
end
{{""}}
{{end -}}

{{range .Macros.Index.MultipleIndices -}}
{{ $moduleName }}.getBy{{ toPascalName .Meta.Name }} = function({{toCamelName .Meta.Name}})
    local res = {}
    local keys = {{$moduleName}}._index.multiple.{{ toCamelName .Meta.Name }}[tostring({{toCamelName .Meta.Name}})]

    for _, k in ipairs(keys) do
        res[k] = {{ $moduleName }}.get(k)
    end

    return res
end
{{""}}
{{end -}}

{{range .Macros.Index.ComplexIndices -}}
{{ $moduleName }}.getBy{{ toPascalName .Name }} = function(
    {{- $funcParamMaxIndex := len .Metas -}}
    {{- range $index, $meta := .Metas -}}
        {{- toCamelName $meta.Name -}}
        {{ if (lt (addInt $index 1) $funcParamMaxIndex) }}, {{end}}
    {{- end -}}
)
    local key = {{""}}
    {{- $funcParamMaxIndex := len .Metas -}}
    {{- range $index, $meta := .Metas -}}
        tostring({{- toCamelName $meta.Name -}}){{- "" -}}
        {{ if (lt (addInt $index 1) $funcParamMaxIndex) }} .. '.' .. {{end}}
    {{- end}}
    return {{$moduleName}}.get({{$moduleName}}._index.complex.{{ toCamelName .Name }}[key])
end
{{""}}
{{end -}}

{{ $moduleName }}._index = {
    unique = {
    {{- range .Macros.Index.UniqueIndices}}
        {{ toCamelName .Meta.Name }} = {
            {{- range $n, $indices := .Datas -}}
                ['{{$n}}'] = {{$indices}},
            {{- end -}}
        },
    {{- end}}
    },

    multiple = {
    {{- range .Macros.Index.MultipleIndices}}
        {{ toCamelName .Meta.Name }} = {
            {{- range $n, $indices := .Datas -}}
                ['{{$n}}'] = { {{range $indices}}{{.}},{{end}} },
            {{- end -}}
        },
    {{- end}}
    },
    
    complex = {
    {{- range .Macros.Index.ComplexIndices}}
        {{ toCamelName .Name }} = {
            {{- range $n, $indices := .Datas -}}
                ['{{$n}}'] = {{$indices}},
            {{- end -}}
        },
    {{- end}}
    }
}

{{ $moduleName }}._data = {
    {{- range $index, $row := .DataRows }}
    [{{$index}}] = {{template "row" $row.Datas}},
    {{- end}}
}
