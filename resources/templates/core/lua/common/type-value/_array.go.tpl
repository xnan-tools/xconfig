{{- $maxIndex := len . -}}
{{- $index := 1 -}}

{ {{range $k, $v := .}}
    {{- $vType := (typeof $v) -}}
    {{- if eq $vType "map[string]interface {}" -}}
        {{- template "common/type-value/_json" $v -}}
    {{- else if eq $vType "[]interface {}" -}}
        {{- template "common/type-value/_array" $v -}}
    {{- else if eq $vType "string" -}}
        "{{$v}}"
    {{- else -}}
        {{- $v -}}
    {{- end -}}
    {{ if (lt $index $maxIndex) }}, {{end}}
    {{- $index = (addInt $index 1) -}}
{{- end}} }