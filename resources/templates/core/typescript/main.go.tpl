// {{template "common/generated-tips"}}
{{"\n"}}

{{- $ns := .Macros.Options.typescript_namespace -}}
{{- $moduleName := toPascalName .Macros.Name -}}
{{- $moduleData := (printf "%s%s" $moduleName "Data") -}}
{{- $space := "" -}}

{{- if $ns -}}
{{- $space = "    " -}}
namespace {{$ns}} {
{{- end -}}

{{$space}}export interface {{$moduleData}} {
{{range .FieldMetas}}
    {{$space}}// {{.Name}} : <{{.TypeName}}> {{.Comment}}
    {{$space}}readonly {{.Name}}: {{template "type" .TypeName}};
{{end}}
{{$space}}}

{{$space}}export class {{$moduleName}} {
    {{$space}}private constructor() {}

    {{$space}}public static get(index: number): {{$moduleData}} {
        {{$space}}return {{$moduleName}}.data[index];
    {{$space}}}

    {{$space}}public static all(): {{$moduleData}}[] {
        {{$space}}return {{$moduleName}}.data;
    {{$space}}}

    {{range .Macros.Index.UniqueIndices -}}
    {{$space}}public static getBy{{ toPascalName .Meta.Name }}({{toCamelName .Meta.Name}}: {{template "type" .Meta.TypeName}}): {{$moduleData}} {
        {{$space}}let key = {{$moduleName}}.{{.Meta.Name}}UniqueIndices[String({{toCamelName .Meta.Name}})];
        {{$space}}return {{ $moduleName }}.get(key);
    {{$space}}}
    {{- end}}

    {{range .Macros.Index.MultipleIndices -}}
    {{$space}}public static getBy{{ toPascalName .Meta.Name }}({{toCamelName .Meta.Name}}: {{template "type" .Meta.TypeName}}): {{$moduleData}}[] {
        {{$space}}let res: {{$moduleData}}[] = [];
        {{$space}}let keys = {{$moduleName}}.{{.Meta.Name}}MultipleIndices[String({{toCamelName .Meta.Name}})];

        {{$space}}for (let index in keys) {
            {{$space}}let keyName = keys[index];
            {{$space}}res[index] = {{ $moduleName }}.get(keyName);
        {{$space}}}

        {{$space}}return res;
    {{$space}}}
    {{- "\n"}}
    {{end -}}

    {{range .Macros.Index.ComplexIndices -}}
    {{$space}}public static getBy{{ toPascalName .Name }}(
        {{- $funcParamMaxIndex := len .Metas -}}
        {{- range $index, $meta := .Metas -}}
            {{- toCamelName $meta.Name -}}
            {{ if (lt (addInt $index 1) $funcParamMaxIndex) }}, {{end}}
        {{- end -}}
    ) {
        {{$space}}let key = {{""}}
        {{- $funcParamMaxIndex := len .Metas -}}
        {{- range $index, $meta := .Metas -}}
            String({{- toCamelName $meta.Name -}}){{- "" -}}
            {{ if (lt (addInt $index 1) $funcParamMaxIndex) }} + '.' + {{end}}
        {{- end}};
        {{$space}}return {{$moduleName}}.get({{$moduleName}}.{{toCamelName .Name}}ComplexIndices[key]);
    {{$space}}}
    {{- end}}

    {{$space}}private static data: {{$moduleData}}[] = [
        {{- range $index, $row := .DataRows }}
        {{$space}}{{template "row" $row.Datas}},
        {{- end}}
    {{$space}}];
    {{""}}
    {{- range .Macros.Index.UniqueIndices}}
    {{$space}}private static {{.Meta.Name}}UniqueIndices = {
        {{- range $n, $indices := .Datas -}}
            "{{$n}}": {{$indices}},
        {{- end -}}
    };
    {{- end -}}
    {{""}}
    {{range .Macros.Index.MultipleIndices}}
    {{$space}}private static {{.Meta.Name}}MultipleIndices = {
        {{- range $n, $indices := .Datas -}}
            "{{$n}}": [ {{range $indices}}{{.}},{{end}} ],
        {{- end -}}
    };
    {{- end -}}
    {{""}}
    {{range .Macros.Index.ComplexIndices}}
    {{$space}}private static {{toCamelName .Name}}ComplexIndices = {
        {{- range $n, $indices := .Datas -}}
            "{{$n}}": {{$indices}},
        {{- end -}}
    };
    {{- end}}
{{$space}}}

{{if $ns -}}
}
{{end}}