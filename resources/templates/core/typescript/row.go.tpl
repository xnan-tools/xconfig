{{- $maxIndex := len . -}}
{
    {{- range $index, $fieldData := . -}}
        {{ $fieldData.Meta.Name }}: {{ valueWithFieldData $fieldData -}}
        {{ if (lt (addInt $index 1) $maxIndex) }}, {{end}}
    {{- end -}}
}