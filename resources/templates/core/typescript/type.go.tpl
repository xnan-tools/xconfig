{{- if eq . "bool" -}}
boolean
{{- else if eq . "int" "uint" "int64" "uint64" "float" "double" -}}
number
{{- else if eq . "string" -}}
string
{{- else -}}
any
{{- end -}}
