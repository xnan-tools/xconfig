package main

import (
	"fmt"
	"os"
	"xconfig/src/app"
)

func main() {
	exec()
}

func exec() {
	if err := app.Run(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
