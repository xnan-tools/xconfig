#!/bin/bash

INPUT_FILE_NAME="./test/actor.xlsx"

if [ -n "${2}" ]; then
    make exec EXEC_ARGS="-l ${1} -t ${2} -o ./test ${INPUT_FILE_NAME}"
else
    make exec EXEC_ARGS="-l ${1} -o ./test ${INPUT_FILE_NAME}"
fi
