CMD=go
BUILD_CMD=$(CMD) build
CLEAN_CMD=$(CMD) clean
TEST_CMD=$(CMD) test

BUILD_DIR=build
BUILD_NAME=xconfig

SRC_DIR=src
RES_DIR=resources

GO_FILES=$(shell find $(SRC_DIR) -type f)
RES_FILES=$(shell find $(RES_DIR) -type f)

all: build

build: $(GO_FILES) copyres
	@echo "Generating executable file..."
	@$(BUILD_CMD) -o $(BUILD_DIR)/$(BUILD_NAME)

release: $(GO_FILES) copyres
	@echo "Generating executable file..."
	@$(BUILD_CMD) -ldflags '-s -w' -o $(BUILD_DIR)/$(BUILD_NAME)

releaseLinux: $(GO_FILES) copyres
	@echo "Generating executable file..."
	@GOOS=linux GOARCH=amd64 $(BUILD_CMD) -ldflags '-s -w' -o $(BUILD_DIR)/$(BUILD_NAME)

copyres: $(RES_FILES)
	@mkdir -p $(BUILD_DIR)/$(RES_DIR)
	@echo "Copying resources..."
	@cp -fr $(RES_DIR) $(BUILD_DIR)

exec: build
	@echo "Executing $(BUILD_NAME)..."
	@./$(BUILD_DIR)/$(BUILD_NAME) $(EXEC_ARGS)
